def m_get_friends(user_id, friends_relationships):
    # Returns array of friends by user_id. Ony level.
    # n - number of friends
    # Complexity: O(n)
    # Memory: O(n)
    #
    n = len(friends_relationships)  # O(1)
    friends = []  # O(1)
    for i in range(n):  # O(n)
        v = friends_relationships[user_id][i]  # O(1)
        if v == 1:  # O(1)
            friends.append(i)  # O(1)
    # Total: O(n)
    return friends


def m_get_friends_unit_tests():
    friends_relationships_1 = [
        [0, 1, 0],
        [0, 0, 0],
        [1, 0, 0]
    ]
    friends_relationships_2 = [
        [0, 1, 0, 1, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 1, 1, 0],
        [1, 1, 1, 1, 1],
        [0, 0, 0, 0, 0]
    ]
    test_cases = [
        ("1", friends_relationships_1, 0, [1]),
        ("2", friends_relationships_2, 0, [1, 3]),
        ("3", friends_relationships_2, 2, [2, 3]),
        ("4", friends_relationships_2, 3, [0, 1, 2, 3, 4]),
        ("5", friends_relationships_2, 4, []),
    ]
    for n, m, user_id, expected_result in test_cases:
        result = m_get_friends(user_id, m)
        assert result == expected_result, "m_get_friends. {0}, {1} != {2}".format(n, expected_result, result)


def m_get_friends_tests():
    m_get_friends_unit_tests()


def m_friends_seen_movies(user_friends, users_movies):
    # Returns # of friends who has seen movies. One level.
    # f - number of friends
    # m - max number of movies has been seen
    # n - number of movies
    # Complexity: O(f*m)
    # Memory: O(n)
    #
    n = len(users_movies)  # O(1)
    friends_seen_movies = [0 for _ in range(n)]  # O(1)
    for friend in user_friends:  # O(f)
        movies = users_movies[friend]  # O(1)
        for i in range(len(movies)):  # O(m)
            current_movie = movies[i]  # O(1)
            if current_movie == 1:  # O(1)
                friends_seen_movies[i] += 1  # O(1)
    # Total: O(f*m)
    return friends_seen_movies


def m_friends_seen_movies_unit_tests():
    users_movies_1 = [
        [1, 0, 0],
        [0, 1, 1],
        [0, 0, 1]
    ]
    users_movies_2 = [
        [1, 0, 1, 0, 1],
        [0, 0, 0, 0, 0],
        [1, 1, 1, 1, 1],
        [1, 0, 1, 0, 0],
        [0, 1, 1, 0, 0],
    ]
    test_cases = [
        ("1", [1], users_movies_1, [0, 1, 1]),
        ("2", [0], users_movies_2, [1, 0, 1, 0, 1]),
        ("3", [1], users_movies_2, [0, 0, 0, 0, 0]),
        ("4", [2], users_movies_2, [1, 1, 1, 1, 1]),
        ("5", [3, 4], users_movies_2, [1, 1, 2, 0, 0])
    ]
    for n, user_friends, users_movies, expected_result in test_cases:
        result = m_friends_seen_movies(user_friends, users_movies)
        assert result == expected_result, "m_friends_seen_movies. {0}, {1} != {2}".format(n, expected_result, result)


def m_friends_seen_movies_tests():
    m_friends_seen_movies_unit_tests()


def m_movies_seen(user_friends, users_movies):
    # Calculates dictionary where key is movie and value is list of friends who have seen this movie.
    # f - number of friends
    # m - number of movies
    # Complexity: O(f*m)
    # Memory: O(f*m)
    #
    movies_seen = dict()  # O(1)
    for friend in user_friends:  # O(f)
        movies = users_movies[friend]  # O(1)
        for i in range(len(movies)):  # O(m)
            current_movie = movies[i]  # O(1)
            if current_movie == 1:  # O(1)
                if i not in movies_seen:  # O(1)
                    movies_seen[i] = set()  # O(1)
                movies_seen[i].add(friend)  # O(1)
    # Total: O(f*m)
    return movies_seen


def m_movies_seen_unit_tests():
    users_movies_1 = [
        [1, 0, 0],
        [0, 1, 1],
        [0, 0, 1]
    ]
    users_movies_2 = [
        [0, 0, 0, 0, 0],
        [0, 1, 1, 0, 0],
        [0, 0, 1, 1, 1],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0]
    ]
    test_cases = [
        ("1", [1], users_movies_1, {1: {1}, 2: {1}}),
        ("2", [0], users_movies_2, {}),
        ("3", [1, 2], users_movies_2, {1: {1}, 2: {1, 2}, 3: {2}, 4: {2}}),
    ]
    for n, user_friends, users_movies, expected_result in test_cases:
        result = m_movies_seen(user_friends, users_movies)
        assert result == expected_result, "m_movies_seen. {0}, {1} != {2}".format(n, expected_result, result)


def m_movies_seen_tests():
    m_movies_seen_unit_tests()


def m_similar_movies(movies_seen, movies_similarities):
    # Calculates similar movies.
    # k - number of movies have been seen by friends
    # l - number of movies
    # n - number of movies
    # Complexity: O(k*l)
    # Memory: O(n)
    #
    n = len(movies_similarities)  # O(1)
    similar_movies = [0 for _ in range(n)]  # O(1)
    for m in movies_seen.keys():  # O(k)
        movies_s = movies_similarities[m]  # O(1)
        k = 0  # O(1)
        total = 0  # O(1)
        for i in range(len(movies_s)):  # O(l)
            mm = movies_s[i]  # O(1)
            if mm == 1:  # O(1)
                total += 1  # O(1)
                if i in movies_seen:  # O(1)
                    k += len(movies_seen[m])  # O(1)
        if total > 0:  # O(1)
            similar_movies[m] = k / total  # O(1)
    # Total: O(k*l)
    return similar_movies


def m_similar_movies_unit_tests():
    movies_similarities_1 = [
        [0, 0, 1],
        [1, 0, 0],
        [1, 1, 0]
    ]
    test_cases = [
        ("1", {1: {1}, 2: {1}}, movies_similarities_1, [0, 0.0, 0.5])
    ]
    for n, movies_seen, movies_similarities, expected_result in test_cases:
        result = m_similar_movies(movies_seen, movies_similarities)
        assert result == expected_result, "m_similar_movies. {0}, {1} != {2}".format(n, expected_result, result)


def m_similar_movies_tests():
    m_similar_movies_unit_tests()


def m_calc_movie(friends_seen_movies, similar_movies):
    # Goes through friends_seen_movies, similar_movies and calculates coefficient for each movie: f/s
    # F = number of friends who have seen this movie.
    # S = mean of the number of similar movies seen for each friend
    # n - number of movies
    # Complexity: O(n)
    # Memory: O(n)
    #
    n = len(friends_seen_movies)  # O(1)
    fs = [0 for _ in range(n)]  # O(1)
    for i in range(n):  # O(n)
        f = friends_seen_movies[i]  # O(1)
        s = similar_movies[i]  # O(1)
        if s != 0:  # O(1)
            fs[i] = f / s  # O(1)
    # print(friends_seen_movies)
    # print(similar_movies)
    r = fs.index(max(fs))  # O(n)
    # Total: O(n)
    return r


def m_calc_movie_unit_tests():
    friends_seen_movies_1 = [0, 1, 1]
    similar_movies_1 = [0, 0.0, 0.5]
    test_cases = [
        ("1", friends_seen_movies_1, similar_movies_1, 2)
    ]
    for n, friends_seen_movies, similar_movies, expected_result in test_cases:
        result = m_calc_movie(friends_seen_movies, similar_movies)
        assert result == expected_result, "m_calc_movie. {0}, {1} != {2}".format(n, expected_result, result)


def m_calc_movie_tests():
    m_calc_movie_unit_tests()


def m_calculation(movies_similarities, friends_relationships, users_movies, user_id=0, debug=False):
    # 1
    if debug:
        print("user_id:", user_id)
    # 2
    user_friends = m_get_friends(user_id, friends_relationships)
    if debug:
        print("friends:", user_friends)
    # 3
    friends_seen_movies = m_friends_seen_movies(user_friends, users_movies)
    if debug:
        print("number of friends who have seen:", friends_seen_movies)
    # 4
    movies_seen = m_movies_seen(user_friends, users_movies)
    if debug:
        print("movies seen by friends:", movies_seen)
    # 5
    similar_movies = m_similar_movies(movies_seen, movies_similarities)
    if debug:
        print("similar movies:", similar_movies)
    # 6
    m = m_calc_movie(friends_seen_movies, similar_movies)
    if debug:
        print("movie:", m)
    return m


def m_calculation_1():
    movies_similarities = [
        [0, 0, 1],
        [1, 0, 0],
        [1, 1, 0]
    ]
    friends_relationships = [
        [0, 1, 0],
        [0, 0, 0],
        [1, 0, 0]
    ]
    users_movies = [
        [1, 0, 0],
        [0, 1, 1],
        [0, 0, 1]
    ]
    m_calculation(movies_similarities, friends_relationships, users_movies, user_id=0, debug=True)


def m_generate_m(n):
    import random
    result = []
    for i in range(n):
        t = []
        result.append(t)
        for j in range(n):
            v = 0
            if random.randint(0, 100) > 90:
                v = 1
            t.append(v)
    return result


def m_calculation_tests():
    debug = True
    import time
    import random

    test_cases = []
    nn = [100, 500]
    for i in range(len(nn)):
        n = nn[i]
        movies_similarities = m_generate_m(n)
        friends_relationships = m_generate_m(n)
        users_movies = m_generate_m(n)
        test_cases.append((str(i), movies_similarities, friends_relationships, users_movies))

    result = []
    for l, movies_similarities, friends_relationships, users_movies in test_cases:
        t0 = time.time()
        user_id = random.randint(0, len(movies_similarities) - 1)
        m = m_calculation(movies_similarities, friends_relationships, users_movies, user_id=user_id, debug=False)
        t1 = time.time()
        total = t1 - t0
        result.append((l, m, total))

    if debug:
        for l, m, total in result:
            print("#:", l, "result:", m, "time:", total)


def m_tests():
    m_get_friends_tests()
    m_friends_seen_movies_tests()
    m_movies_seen_tests()
    m_similar_movies_tests()
    m_calc_movie_tests()
    m_calculation_tests()


def m_comments():
    t1 = '''
Introduction.  

Lets represents input data.

Movies similarities - matrix. If ms[i][j] == 1 then movie with id i similar movie with id j.
For example:
movies_similarities = [
        [0, 0, 1],
        [1, 0, 0],
        [1, 1, 0]
    ]
Movie 2 is similar with 0, 1.

Relationships for friends - matrix. If fr[i][j] == 1 then user with id i has relation with user with id j.
For example:
friends_relationships = [
        [0, 1, 0],
        [0, 0, 0],
        [1, 0, 0]
    ]
Friend 0 has relation with 1.

History for users and movies (has already seen) - matrix. If um[i][j] == 1 then user with id i has seen movie with id j.
For example: 
users_movies = [
        [1, 0, 0],
        [0, 1, 1],
        [0, 0, 1]
    ]
User with id 0 has seen 0 movie.
User with id 1 has seen 1, 2 movies.    

Algorithm.   

1) 
Get user id. For this one algorithm should return recommendation of movie.
For example. 
user_id: 0.

2) 
For this user find direct friends.
Read relations from one row from friends_relationships matrix.
Get user_friends - list where elements are index/id of friends.
For given example (m_get_friends). 
friends: [1].

3) 
For each friend of user get movies have been seen. 
If movie has been seen then increase counter by 1 in array with result. Result is array where index is id of movie 
and value total number of views for all friends for that movie.
For given example (m_friends_seen_movies).
friends_seen_movies: [0, 1, 1].
This is F. F = number of friends who have seen this movie.    

4) 
Calculate dictionary where key is movie and value is list of friends who have seen this movie.
For given example (m_movies_seen).
{1: {1}, 2: {1}}  

5) 
Calculate similar movies.
Go through dictionary of movies and list of friends.
For each movie get list of similar movies.
Go through similar movies:
If current movie exists in dictionary of movies and list of friends then add number of users to counter - k
Calculate coefficient = k/total for each movie.
For given example (m_similar_movies):
similar_movies: [0, 0.0, 0.5].  
S = mean of the number of similar movies seen for each friend

6) 
Go through friends_seen_movies, similar_movies and calculate coefficient for each movie: f/s
Return max from all f/s.
For given example (m_calc_movie):
2.

Details.
1) Step 1
2) Step 2
m_get_friends  
Complexity: O(n), Memory: O(n)  
3) Step 3
m_friends_seen_movies
Complexity: O(f*m), Memory: O(n)
4) Step 4
m_movies_seen
Complexity: O(f*m), Memory: O(f*m)
5) Step 5
m_similar_movies 
Complexity: O(k*l), Memory: O(n)
6) Step 6
m_calc_movie
Complexity: O(n), Memory: O(n)

Total:
Complexity: O(n) + O(f*m) + O(f*m) + O(k*l) + O(n)
Memory: O(n) + O(n) + O(f*m) + O(n) + O(n)

Results:
Algorithm is very simple but results cun be interpreted.
Performance not good but can be improve: through || calculations.  

'''


def tt1():
    m1 = m_generate_m(3)
    print(m1)


# m_tests()

# m_get_friends_tests()

# m_friends_seen_movies_tests()

# m_movies_seen_tests()

# m_similar_movies_tests()

# m_calc_movie_tests()

# m_calculation_1()

# tt1()

m_calculation_tests()
